import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class MainApp 
{

	//add map as an instance variable so it is visible across methods
	private HashMap<Customer, ArrayList<Product>> salesMap;
	
	public static void main(String[] args) 
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}


	private void start() 
	{
		initializeMap();
		loadData();
		demoMethods();
	}
	
	private void demoMethods() 
	{
		//make the customer object and use as key for the query
		Customer c1 = new Customer("jane", "jones", "jj@gmail.com");
		ArrayList<Product> pList = this.salesMap.get(c1);
		
		//now call our methods to get total, average etc
		double total = getTotalCost(pList);
		System.out.println("total: " + total);
		
		double average = getAverageCost(pList);
		System.out.println("average: " + average);
		
		System.out.println("Sorted...");
		sortBy(pList, new ProductPriceComparator());	
		System.out.println(pList);
		
		System.out.println("Filtered...");
		ArrayList fList = filterBy(pList, new ProductTypeFilter(ProductType.Book));
		System.out.println(fList);
	}

	private void loadData() 
	{
		Customer c1 = new Customer("jane", "jones", "jj@gmail.com");
		ArrayList<Product> list1 = new ArrayList<Product>();
		
		list1.add(new Product("ipad air 2", 749, "E12345", 
				ProductType.Electronic));
		
		list1.add(new Product("The Revenant", 12.99, "M54321", 
				ProductType.Movie));
		
		list1.add(new Product("Big Java Late Objects", 45, "B76543", 
				ProductType.Book));
		
		this.salesMap.put(c1, list1);
		
		//add more customers and products if required...
		
	}

	private void initializeMap() 
	{
		this.salesMap = new HashMap<Customer, ArrayList<Product>>();
		
	}

	public ArrayList<Product> filterBy(ArrayList<Product> list,
			IFilter filter)
	{
		ArrayList<Product> rList = new ArrayList<Product>();
		for(Product p : list)
		{
			if(filter.matches(p))
				rList.add(p);
		}
		
		return rList;
	}

	public ArrayList<Product> filterByType(ArrayList<Product> list,
			ProductType type)
	{
		ArrayList<Product> rList = new ArrayList<Product>();
		for(Product p : list)
		{
			if(type == p.getProductType())
				rList.add(p);
		}
		
		return rList;
	}

	public void sortBy(ArrayList<Product> list,
							Comparator<Product> comp)
	{
		Collections.sort(list, comp);
	}
	
	public double getTotalCost(ArrayList<Product> list)
	{
		double sum = 0;
		for(Product p : list)
		{
			sum += p.getPrice();
		}
		return sum;
	}
	
	public double getAverageCost(ArrayList<Product> list)
	{
		double sum = getTotalCost(list);
		return sum/list.size();
		
	//	return getTotalPrice(list)/list.size();
	}
}







